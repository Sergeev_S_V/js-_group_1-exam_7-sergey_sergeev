import React from 'react';
import ReceivedOrder from "../../Components/ReceivedOrder";
import './OrderDetailsBlock.css';

const OrderDetailsBlock = (props) => {
  let totalPrice = props.totalPrice.totalPrice;
  let titleClass = 'title';
  let totalPriceClass = 'totalPrice';
  if (totalPrice > 0) {
    titleClass = 'hide';
  } else {
    titleClass = 'title';
    totalPriceClass = 'hide';
  }

  return (
    <div className='orderDetailsBlock'>
      <ReceivedOrder totalPrice={props.totalPrice}
                     amount={props.amount}
                     remove={props.remove}/>
      <div className={totalPriceClass}>Total price: {totalPrice} KGS</div>

      <div className={titleClass}>
        <p>Order is empty!</p>
        <p>Please add some items!</p>
      </div>
    </div>
  );
};

export default OrderDetailsBlock;