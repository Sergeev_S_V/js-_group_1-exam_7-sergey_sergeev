import React from 'react';
import Products from "../../Components/Products";
import './AddItemsBlock.css';

const AddItemBlock = (props) => {
  return (
    <div className='addItemsBlock'>
      <Products click={props.click}/>
    </div>
  );
};

export default AddItemBlock;