import React, { Component } from 'react';
import './App.css';
import OrderDetailsBlock from "./OrderDetailsBlock/OrderDetailsBlock";
import AddItemBlock from "./AddItemsBlock/AddItemsBlock";

class App extends Component {

  state = {
    amountProducts: {
      Hamburger: 0,
      Cheeseburger: 0,
      Fries: 0,
      Coffee: 0,
      Tea: 0,
      Cola: 0
    },
    totalPriceProducts: {
      totalPrice: 0,
      Hamburger: 0,
      Cheeseburger: 0,
      Fries: 0,
      Coffee: 0,
      Tea: 0,
      Cola: 0
    }
  };

  price = {
    Hamburger: 80,
    Cheeseburger: 90,
    Fries: 45,
    Coffee: 70,
    Tea: 50,
    Cola: 40
  };

  orderProduct = (name) => {
    let products = {...this.state};
    products.amountProducts[name]++;
    products.totalPriceProducts[name] += this.price[name];
    products.totalPriceProducts.totalPrice += this.price[name];

    this.setState(products);
  };

  remove = (name) => {
    let amount = {...this.state};
    amount.totalPriceProducts.totalPrice -= amount.totalPriceProducts[name];
    amount.totalPriceProducts[name] = 0;
    amount.amountProducts[name] = 0;

    this.setState(amount);
  };



  render() {
    return (
      <div className="App">
        <OrderDetailsBlock totalPrice={this.state.totalPriceProducts}
                           amount={this.state.amountProducts}
                           remove={this.remove}/>
        <AddItemBlock click={this.orderProduct}/>
      </div>
    );
  }
}

export default App;
