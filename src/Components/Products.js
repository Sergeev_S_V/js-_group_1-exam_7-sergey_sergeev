import React from 'react';
import Product from "./Product";

const Products = (props) => {
  return (
    <div className='products'>
      <Product click={() => props.click('Hamburger')} name='Hamburger' price='80'/>
      <Product click={() => props.click('Coffee')} name='Coffee' price='70'/>
      <Product click={() => props.click('Cheeseburger')} name='Cheeseburger' price='90'/>
      <Product click={() => props.click('Fries')} name='Fries' price='45'/>
      <Product click={() => props.click('Tea')} name='Tea' price='50'/>
      <Product click={() => props.click('Cola')} name='Cola' price='40'/>
    </div>
  );
};

export default Products;