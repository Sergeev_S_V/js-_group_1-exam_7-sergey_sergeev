import React from 'react';
import './ReceivedProduct.css';

const ReceivedProduct = (props) => {
  return (
    <div className='receivedProduct' onClick={props.click}>
      <div className='remove'><button onClick={props.remove} className='btn'> Delete order</button></div>
      <p className='name'>{props.name} x {props.amount}</p>
      <p className='price'>{props.price} KGS</p>
    </div>
  );
};

export default ReceivedProduct;