import React from 'react';
import ReceivedProduct from "./ReceivedProduct";

const ReceivedOrder = (props) => {
  let totalPrice = props.totalPrice;

  let receivedOrderClass = 'receivedOrder';
  if (totalPrice.totalPrice === 0) {
    receivedOrderClass = 'hide';
  } else {
    receivedOrderClass = 'receivedOrder';
  }

  let products = props.amount;

  let productsName = Object.keys(products);

  let receivedHamburger = [];
  let receivedCheeseburger = [];
  let receivedFries = [];
  let receivedCoffee = [];
  let receivedTea = [];
  let receivedCola = [];


  productsName.map(nameOfProduct => {
    let amountOfProduct = products[nameOfProduct];
    for (let i = 0; i < amountOfProduct; i++) {
      if (nameOfProduct === 'Hamburger') {
        if (receivedHamburger.length === 0) {
          receivedHamburger.push(
            <ReceivedProduct name={nameOfProduct}
                             amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => props.remove('Hamburger')}
                             key={Date.now()}
            />);
        }
      } else if (nameOfProduct === 'Cheeseburger') {
        if (receivedCheeseburger.length === 0) {
          receivedCheeseburger.push(
            <ReceivedProduct name={nameOfProduct}
                             amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => {props.remove('Cheeseburger')}}
                             key={Date.now()}
            />);
        }
      } else if (nameOfProduct === 'Fries') {
        if (receivedFries.length === 0) {
          receivedFries.push(
            <ReceivedProduct name={nameOfProduct} amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => props.remove('Fries')}
                             key={Date.now()}
            />);
        }
      } else if (nameOfProduct === 'Coffee') {
        if (receivedCoffee.length === 0) {
          receivedCoffee.push(
            <ReceivedProduct name={nameOfProduct} amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => props.remove('Coffee')}
                             key={Date.now()}
            />);
        }
      } else if (nameOfProduct === 'Tea') {
        if (receivedTea.length === 0) {
          receivedTea.push(
            <ReceivedProduct name={nameOfProduct} amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => props.remove('Tea')}
                             key={Date.now()}
            />);
        }
      } else if (nameOfProduct === 'Cola') {
        if (receivedCola.length === 0) {
          receivedCola.push(
            <ReceivedProduct name={nameOfProduct} amount={amountOfProduct}
                             price={totalPrice[nameOfProduct]}
                             remove={() => props.remove('Cola')}
                             key={Date.now()}
            />);
        }
      }

    }
  });

  return (
    <div className={receivedOrderClass} >
      {receivedHamburger}
      {receivedCheeseburger}
      {receivedFries}
      {receivedCoffee}
      {receivedTea}
      {receivedCola}
    </div>
  );
};

export default ReceivedOrder;