import React from 'react';
import './Product.css';

const Product = (props) => {
  return (
    <div className='product' onClick={props.click}>
      <p>{props.name}</p>
      <p>Price: {props.price} KGS</p>
    </div>
  );
};

export default Product;